module Main exposing (Expr(..), Model, Msg(..))

import Html
import Html.Events exposing (onClick, onInput)
import Http



-- DATA --


type Expr
    = Val Int
    | Var String
    | Plus Expr Expr
    | Times Expr Expr



-------------------- Assignment 1 --------------
-- parse : String -> Maybe Expr
-- show : Expr -> String
------------------- Assignment 2 -----------------


type alias Model =
    { expr : Maybe Expr
    , simplified : String
    }


type Msg
    = Parse String
    | Simplified (Result Http.Error String)



-- main =
--     Html.program
--         { init = init
--         , view = view
--         , update = update
--         , subscriptions = subscriptions
--         }
