# Week 8 Elm@FRI



## Assignment 1

Izraze bomo predstavili s sledečim podatkovnim tipom:


```elm
type Expr
    = Val Int
    | Var String
    | Plus Expr Expr
    | Times Expr Expr
```
Npr. izraz x*y+5 lahko predstavimo kot Plus (Times (Var x) (Var y) ) (Val 5).

Najprej napišite funkcijo, ki bo iz niza zgradila Expr. V nizu bo podan izraz v postfiksni obliki, npr. "x y + 5 *" predstavlja izraz Plus (Times (Var x) (Var y) ) (Val 5).

```elm
parse : String -> Maybe Expr
```

Druga funkcija pa naj iz izraza sestavi običajen prikaz izraza (v infiksni obliki). Predhodno omenjeni izraz naj bo torej predstavljen kot x*y+5.



```elm
show : Expr -> String
```


## Assignment 2.

Pri drugi nalogi boste uporabljali oddaljen API za poenostavljanje izrazov. Uporabnik naj podaja izraze v postfiksni obliki v tekstovno polje, program naj ga pretvori v 'Expr', potem pa pošlje na oddaljen strežnik, ki ga vrne poenostavljenega.

Uporabite lahko samo dva tipa sporočil:

```elm
type Msg
    = Parse String
    | Simplified (Result Http.Error String)
```
Poenostavljanje boste izvajali z API-jem dostopnim na: https://newton.now.sh/simplify/

Temu naslovi preprosto dodate niz vašega izraza (v infiksni obliki).

Primer izvajanja programa si lahko ogledate na spodnjih gibljivih slikah.

![](example.gif)
